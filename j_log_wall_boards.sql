BEGIN
  dbms_scheduler.create_job(job_name        => 'j_log_wall_boards'
                           ,job_type        => 'STORE_PROCEDURE'
                           ,job_action      => 'dba_sosdw.p_llenar_log'
                           ,start_date      => '11/02/2020 23:59:00 -03:00'
                           ,repeat_interval => 'FREQ=DAYLI;INTERVAL=1'
                           ,end_date        => NULL
                           ,auto_drop       => FALSE
                           ,enabled         => TRUE);
END;
